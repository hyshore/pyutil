#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from lxml import etree
import re
import json

quota_pattern = re.compile(r'(\d+)GB/(\d+)GB$')

register_nodes = [
]

def get_html():
    with open("/Users/yanghaixiang/nodes.html") as fi:
        return fi.read()

def get_nodes():
    html_str = get_html()
    if html_str is None:
        return None
    html = etree.HTML(html_str)
    if not html:
        return None
    elements = html.xpath('//*[@id="class-0"]//div[contains(@class, "card")]')
    nodes = {}
    for ele in elements:
        title = ele.xpath('.//h2/text()')
        quota = ele.xpath('.//h2//span/text()')
        status = ele.xpath('.//span[contains(@class, "status-indicator")]')
        ele_type = ele.xpath('.//div[contains(@class, "ribbon")]/text()')
        node = {}
        if len(title) > 0:
            node['name'] = strip_str(title[0])
            print(node['name'])
        quota_vals = parse_quota(strip_str(quota[0]))
        if quota_vals:
            node['used'] = quota_vals[0]
            node['cap'] = quota_vals[1]
        if len(ele_type) > 0:
            node['type'] = strip_str(ele_type[0])
        if len(status) > 0:
            cls = status[0].get("class")
            if 'status-green' in cls:
                node['status'] = 'green'
            elif 'status-yellow' in cls:
                node['status'] = 'yellow'
            elif 'status-red' in cls:
                node['status'] = 'red'
            else:
                node['status'] = 'unknown'
        valid = True
        for key in ['name', 'type', 'used', 'cap', 'status']:
            if node.get(key) is None:
                valid = False

        if valid:
            nodes[gen_key(node)] = node
    return nodes

def gen_key(node):
    return node['name'] + '_' + node['type']
def strip_str(s):
    return s.replace("\n", '').replace('\t', '').replace(" ", "").replace("\u00a0", "")

def parse_quota(quota):
    match = quota_pattern.match(quota)
    if match is not None:
        return [int(match.group(1)), int(match.group(2))]
    return None


def main():
    nodes = get_nodes()
    print(json.dumps(nodes, ensure_ascii=False))

if __name__ == "__main__":
    main()

